# Tower Defence Base
## Описание
### Описание прототипа
Основа игры в стиле Tower defence.
Враги двигаются к цели и атакуют ее на необходимой дистанции.
Стреляющие башни стреляют по врагам, стараясь недопустить их к цели.

### Описание механик
Архитектура основана на использовании методов интерфейсов.
Практически каждый компонент является реализацией некого интерфейса. 

Стрельба может быть простой, в текущую позицию игрока, с упреждением, с учетом балистики ядра, со слежением цели.
Противники могут передвигаться прямо на цель, по навигационной сетке.
Основной компонент объекта хранит в себе ссылки на эти релизованные компоненты и использует их, не зная деталей их реализации.

Расширение проекта сводится к реализациям написанных интерфейсов при создании нового компонента.

## Скриншоты
![Screen1](/Readme/Screenshots/1.PNG)
![Screen2](/Readme/Screenshots/2.PNG)