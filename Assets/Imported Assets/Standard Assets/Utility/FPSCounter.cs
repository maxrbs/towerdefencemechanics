using UnityEngine;
using UnityEngine.UI;

namespace UnityStandardAssets.Utility
{
    [RequireComponent(typeof (Text))]
    public class FPSCounter : MonoBehaviour
    {
        private const string DISPLAY_TEMPLATE = "{0} FPS";

        [SerializeField] private float fpsMeasurePeriod = 0.5f;

        private float nextMeasureTime = 0;
        private Text text;


        private void Start()
        {
            nextMeasureTime = Time.realtimeSinceStartup + fpsMeasurePeriod;
            text = GetComponent<Text>();
        }


        private void Update()
        {
            // measure average frames per second

            if (Time.realtimeSinceStartup > nextMeasureTime)
            {
                int currentFps = (int) (1 / Time.deltaTime);

                nextMeasureTime += fpsMeasurePeriod;

                text.text = string.Format(DISPLAY_TEMPLATE, currentFps);
            }
        }
    }
}
