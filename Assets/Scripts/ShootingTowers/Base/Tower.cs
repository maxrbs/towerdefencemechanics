using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Tower : MonoBehaviour
{
    protected IShootable shootingComponent;
    protected ITargetFindable targetFinderComponent;
    protected IAimCalculatable aimCalculatorComponent;

    protected Transform targetToDefend;
    protected Transform targetToAttack;

    [SerializeField] protected Transform projectileSpawnPoint;

    public abstract void InitComponents();

    public abstract void Setup(TowerData towerData);

}
