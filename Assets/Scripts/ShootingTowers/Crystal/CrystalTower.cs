﻿using UnityEngine;

public class CrystalTower : Tower
{
    public override void InitComponents()
    {
        shootingComponent = new DirectedShooting();
        targetFinderComponent = new PoolEnemiesFinder(transform);
        aimCalculatorComponent = new CurrentTargetPositionAimCalculator();
    }

    public override void Setup(TowerData towerData)
    {
        targetToDefend = ServiceLocator.Current.Get<TargetToDefend>().transform;

        (shootingComponent as DirectedShooting).Init(towerData, projectileSpawnPoint);
        (targetFinderComponent as PoolEnemiesFinder).Init(towerData.ShootingDistance);
        (aimCalculatorComponent as CurrentTargetPositionAimCalculator).Init(shootingComponent);
    }

    private void Update()
    {
        if (targetToDefend == null)
            targetToAttack = targetFinderComponent.GetTarget(0);
        else
            targetToAttack = targetFinderComponent.GetClosestTo(targetToDefend.position);


        if (targetToAttack == null)
            return;

        shootingComponent.GetShootingStartPoint().rotation = aimCalculatorComponent.CalculateAimDirection(targetToAttack);

        if (CanShoot())
            shootingComponent.Shoot();
    }

    private bool CanShoot()
    {
        if (Vector3.Distance(shootingComponent.GetShootingStartPoint().position, targetToAttack.position) > shootingComponent.GetShootingRange())
            return false;

        return true;
    }
}
