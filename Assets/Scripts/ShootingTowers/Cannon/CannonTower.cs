﻿using UnityEngine;

[RequireComponent(typeof(CannonRotator))]
public class CannonTower : Tower
{
    [SerializeField ] private CannonRotator cannonRotator;

    public override void InitComponents()
    {
        cannonRotator = GetComponent<CannonRotator>();

        aimCalculatorComponent = new PredictingTargetPositionWithGravityAimCalculator();
        shootingComponent = new DirectedShooting();
        targetFinderComponent = new TagEnemyFinder(transform);
    }

    public override void Setup(TowerData towerData)
    {
        targetToDefend = ServiceLocator.Current.Get<TargetToDefend>().transform;

        (shootingComponent as DirectedShooting).Init(towerData, projectileSpawnPoint);
        (targetFinderComponent as TagEnemyFinder).Init(towerData.ShootingDistance, "Enemy");
        (aimCalculatorComponent as PredictingTargetPositionWithGravityAimCalculator).Init(shootingComponent, false);
        
        cannonRotator.Init(aimCalculatorComponent);
    }


    private void Update()
    {
        if (targetFinderComponent.GetTargetsCount() > 0)
        {
            if (targetToDefend == null)
                targetToAttack = targetFinderComponent.GetTarget(0);
            else
                targetToAttack = targetFinderComponent.GetClosestTo(targetToDefend.position);
        }

        if (targetToAttack == null)
            return;

        cannonRotator.AimTo(targetToAttack);

        if (CanShoot())
            shootingComponent.Shoot();
    }

    private bool CanShoot()
    {
        if (!cannonRotator.AimedTo(targetToAttack))
            return false;

        if (Vector3.Distance(transform.position, targetToAttack.position) > shootingComponent.GetShootingRange())
            return false;

        return true;
    }
}
