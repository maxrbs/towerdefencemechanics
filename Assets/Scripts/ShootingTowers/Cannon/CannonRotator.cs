using UnityEngine;

public class CannonRotator : MonoBehaviour
{
    private const float ACCURACY = .5f;

    [Tooltip("����� ����� ��� �������� �����/����")]
    [SerializeField] private Transform pitchPart;

    [Tooltip("����� ����� ��� �������� ��/������ ������� �������")]
    [SerializeField] private Transform yawPart;

    [SerializeField] private float rotationMaxDegreesBySecond;

    private IAimCalculatable aimCalculator;

    private float aimCalculatePeriod = 0.05f;
    private float nextCalculateTime = 0f;
    private Vector3 aimDirection;

    public void Init(IAimCalculatable aimCalculatorComponent)
    {
        aimCalculator = aimCalculatorComponent;
    }


    public bool AimedTo(Transform target)
    {
        TryUpdateAimDirection(target);
        
        Quaternion pitchTargetAim = Quaternion.Euler(aimDirection.x, aimDirection.y, pitchPart.rotation.z);

        float difference = Quaternion.Angle(pitchPart.rotation, pitchTargetAim);

        if (difference <= ACCURACY)
            return true;
        
        return false;
    }

    public void AimTo(Transform target)
    {
        TryUpdateAimDirection(target);

        Quaternion pitchTargetAim = Quaternion.Euler(aimDirection.x, aimDirection.y, pitchPart.rotation.z);
        Quaternion yawTargetAim = Quaternion.Euler(yawPart.rotation.x, aimDirection.y, yawPart.rotation.z);

        var pitchRotation = Quaternion.RotateTowards(pitchPart.rotation, pitchTargetAim, rotationMaxDegreesBySecond * Time.deltaTime);
        var yawRotation = Quaternion.RotateTowards(yawPart.rotation, yawTargetAim, rotationMaxDegreesBySecond * Time.deltaTime);
        
        yawPart.rotation = yawRotation;
        pitchPart.rotation = pitchRotation;
    }

    private void TryUpdateAimDirection(Transform target)
    { 
        if (Time.time > nextCalculateTime)
        {
            nextCalculateTime = Time.time + aimCalculatePeriod;
            
            aimDirection = aimCalculator.CalculateAimDirection(target).eulerAngles;
        }
    }

}
