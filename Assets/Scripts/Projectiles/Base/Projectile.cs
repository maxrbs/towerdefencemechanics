using UnityEngine;

public abstract class Projectile : MonoBehaviour
{
    protected IMovable moveComponent;
    protected ITargetFindable enemiesFinderComponent;
    protected ITargetBasedAttackable attackComponent;

    protected Vector3 startPosition;

    public abstract void InitComponents();
    public abstract void Setup(float damage, float attackDistance, float speed);
    

    protected bool ReachedBorders()
    {
        float floorLevelHeight = -10f;
        float maxFlightDistance = 200f;

        if (transform.position.y < floorLevelHeight)
            return true;

        if (Vector3.Distance(startPosition, transform.position) > maxFlightDistance)
            return true;

        return false;
    }

    public void DestroySelf()
    {
        ServiceLocator.Current.Get<GameObjectsPool>().Release(gameObject);
        //Destroy(gameObject);
    }

    public IMovable GetMoveComponent()
    {
        return moveComponent;
    }

    public ITargetFindable GetTargetFinder()
    {
        return enemiesFinderComponent;
    }

    public ITargetBasedAttackable GetAttackComponent()
    { 
        return attackComponent;
    }
}
