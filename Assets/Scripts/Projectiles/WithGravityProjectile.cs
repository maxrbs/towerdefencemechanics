using UnityEngine;

public class WithGravityProjectile : Projectile
{
    public override void InitComponents()
    {
        moveComponent = new DirectedForwardMovement(transform);
        attackComponent = new ExplosiveAttack(transform);
        enemiesFinderComponent = new TagEnemyFinder(transform);
    }

    public override void Setup(float damage, float attackDistance, float speed)
    {
        Rigidbody rigidbody = gameObject.GetComponent<Rigidbody>();        
        if (rigidbody == null)
            rigidbody = gameObject.AddComponent<Rigidbody>();
        rigidbody.useGravity = true;
        rigidbody.isKinematic = false;
        rigidbody.velocity = Vector3.zero;

        startPosition = transform.position;

        (attackComponent as ExplosiveAttack).Init(damage, attackDistance);
        (moveComponent as DirectedForwardMovement).Init(speed);
        (enemiesFinderComponent as TagEnemyFinder).Init(attackDistance, "Enemy");
    }


    private void Update()
    {
        if (ReachedBorders())
        {
            DestroySelf();
        }

        if (enemiesFinderComponent.GetTargetsCount() > 0)
        {
            Transform closest = enemiesFinderComponent.GetClosestTo(transform.position);

            if (Vector3.Distance(closest.position, transform.position) < attackComponent.GetAttackMinDistance())
            {
                attackComponent.Attack(closest);

                DestroySelf();
            }
        }

        moveComponent.Move();

    }
}
