using UnityEngine;

public class GuidedProjectile : Projectile
{
    private Transform target;

    public override void InitComponents()
    {
        moveComponent = new DirectedForwardMovement(transform);
        attackComponent = new ExplosiveAttack(transform);
        enemiesFinderComponent = new TagEnemyFinder(transform);
    }

    public override void Setup(float damage, float attackDistance, float speed)
    {
        Rigidbody rigidbody = gameObject.GetComponent<Rigidbody>();
        if (rigidbody == null)
            rigidbody = gameObject.AddComponent<Rigidbody>();
        rigidbody.useGravity = false;
        rigidbody.isKinematic = true;

        startPosition = transform.position;
        float searhingDistance = 20f;

        (attackComponent as ExplosiveAttack).Init(damage, attackDistance);
        (moveComponent as DirectedForwardMovement).Init(speed);
        (enemiesFinderComponent as TagEnemyFinder).Init(searhingDistance, "Enemy");
        
    }

    private void Update()
    {
        if (ReachedBorders())
            DestroySelf();

        if (enemiesFinderComponent.GetTargetsCount() > 0)
        {
            target = enemiesFinderComponent.GetClosestTo(transform.position);
        }
        else
        {
            moveComponent.Move();
            return;
        }

        if (Vector3.Distance(transform.position, target.position) < attackComponent.GetAttackMinDistance())
        {
            attackComponent.Attack(target);
            DestroySelf();
        }
        else
        {
            transform.rotation = GetLookRotation(target);
            moveComponent.Move();
        }
    }

    private Quaternion GetLookRotation(Transform target)
    {
        Vector3 direction = target.position - transform.position;

        Quaternion directionQuaternion = Quaternion.LookRotation(direction);

        float rotationSpeed = moveComponent.GetSpeed() * 10f * Time.deltaTime;

        return Quaternion.RotateTowards(transform.rotation, directionQuaternion, rotationSpeed);
    }
}
