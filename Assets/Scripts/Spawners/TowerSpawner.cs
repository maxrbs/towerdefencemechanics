using System.Collections.Generic;
using UnityEngine;

public class TowerSpawner : IService
{
    private List<TowerData> towerDataList;

    public void Init(TowerSpawnerData towerSpawnerData)
    {
        towerDataList = towerSpawnerData.TowerDataList;
    }

    public void SpawnTowers()
    {
        foreach (TowerData towerData in towerDataList)
        {
            SpawnTower(towerData);
        }
    }

    private void SpawnTower(TowerData towerData)
    {
        GameObjectsPool objectsPool = ServiceLocator.Current.Get<GameObjectsPool>();

        GameObject prefab = Resources.Load(towerData.TowerPath) as GameObject;

        GameObject spawnedTower = objectsPool.Get(prefab);

        spawnedTower.transform.position = new Vector3(towerData.x, towerData.y, towerData.z);

        Tower towerClass = spawnedTower.GetComponent<Tower>();
        towerClass.InitComponents();
        towerClass.Setup(towerData);
    }
}