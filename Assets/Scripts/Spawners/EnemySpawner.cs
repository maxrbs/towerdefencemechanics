﻿using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour, IService
{
    private List<EnemyData> enemiesToSpawn;

    private float spawnPeriod;
    private int enemiesCountToSpawn;

    private Vector3 spawnPosition;
    private float nextSpawnTime = 0f;
    private int spawnedEnemiesCount = 0;


    public void Init(EnemySpawnerData enemySpawnerData)
    {
        enemiesToSpawn = enemySpawnerData.EnemyList;

        spawnPeriod = enemySpawnerData.SpawnPeriod;
        enemiesCountToSpawn = enemySpawnerData.CountToSpawn;

        spawnPosition = new Vector3(enemySpawnerData.x, enemySpawnerData.y, enemySpawnerData.z);
    }


    void Update()
    {
        if (ServiceLocator.Current.Get<GameStateMachine>().GetState() == State.Paused)
            return;

        if (CanSpawn())
            Spawn();
    }

    private bool CanSpawn()
    {
        if (ServiceLocator.Current.Get<TargetToDefend>() == null)
            return false;

        if (Time.time <= nextSpawnTime)
            return false;

        if (spawnedEnemiesCount >= enemiesCountToSpawn)
            return false;

        return true;
    }

    private void Spawn()
    {
        nextSpawnTime = Time.time + spawnPeriod;
        spawnedEnemiesCount++;

        int index = Random.Range(0, enemiesToSpawn.Count);
        EnemyData enemyData = enemiesToSpawn[index];

        GameObject prefabToSpawn = Resources.Load(enemyData.PrefabPath) as GameObject;

        GameObjectsPool objectsPool = ServiceLocator.Current.Get<GameObjectsPool>();

        GameObject spawnedEnemy = objectsPool.Get(prefabToSpawn);

        spawnedEnemy.transform.position = spawnPosition;

        Enemy enemyClass = spawnedEnemy.GetComponent<Enemy>();
        enemyClass.InitComponents();
        enemyClass.Setup(enemyData);

    }
}
