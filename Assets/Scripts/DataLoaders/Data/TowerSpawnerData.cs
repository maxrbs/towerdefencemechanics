﻿using System.Collections.Generic;

[System.Serializable]
public class TowerSpawnerData
{
    public List<TowerData> TowerDataList;
}