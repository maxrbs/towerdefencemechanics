[System.Serializable]
public class TowerData
{
    public string TowerPath;
    public string ProjectilePath;

    public float ProjectileDamage;
    public float ProjectileSpeed;
    public float ProjectileRadius;

    public float ShootingDistance;
    public float ShootPeriod;

    public float x;
    public float y;
    public float z;
}
