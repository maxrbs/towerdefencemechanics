[System.Serializable]
public class EnemyData
{
    public string PrefabPath;
    public float Damage;
    public float Speed;
    public float StartHealth;
    public float AttackDistance;
}
