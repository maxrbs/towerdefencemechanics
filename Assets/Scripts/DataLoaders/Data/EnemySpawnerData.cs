using System.Collections.Generic;

[System.Serializable]
public class EnemySpawnerData
{
    public List<EnemyData> EnemyList;
    public float SpawnPeriod;
    public int CountToSpawn;

    public float x;
    public float y;
    public float z;
}
