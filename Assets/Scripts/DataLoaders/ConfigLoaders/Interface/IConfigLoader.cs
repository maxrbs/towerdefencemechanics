public interface IConfigLoader
{
    public EnemySpawnerData GetEnemySpawnerData();
    public TowerSpawnerData GetTowerSpawnerData();
    public DefencingTargetData GetDefencingTargetData();

}
