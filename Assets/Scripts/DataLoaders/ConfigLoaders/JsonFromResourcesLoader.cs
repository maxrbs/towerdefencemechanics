using System.IO;
using UnityEngine;

public class JsonFromResourcesLoader : MonoBehaviour, IConfigLoader
{
    private const string DEFENCING_TARGET_CONFIG_NAME = "DefencingTargetConfig.json";
    private const string ENEMY_SPAWNER_CONFIG_NAME = "EnemySpawnerConfig.json";
    private const string TOWER_SPAWNER_CONFIG_NAME = "TowerSpawnerConfig.json";

    private string path = "/Resources/Configs/";

    [SerializeField] private string foulderName = "levelfoulder/";


    private void Start()
    {
        GetEnemySpawnerData();
        GetDefencingTargetData();
        GetTowerSpawnerData();
    }

    public DefencingTargetData GetDefencingTargetData()
    {
        string finalPath = Application.dataPath + path + foulderName + DEFENCING_TARGET_CONFIG_NAME;

        string text = File.ReadAllText(finalPath);
        return JsonUtility.FromJson<DefencingTargetData>(text);
    }

    public EnemySpawnerData GetEnemySpawnerData()
    {
        string finalPath = Application.dataPath + path + foulderName + ENEMY_SPAWNER_CONFIG_NAME;

        string text = File.ReadAllText(finalPath);
        return JsonUtility.FromJson<EnemySpawnerData>(text);
    }

    public TowerSpawnerData GetTowerSpawnerData()
    {
        string finalPath = Application.dataPath + path + foulderName + TOWER_SPAWNER_CONFIG_NAME;

        string text = File.ReadAllText(finalPath);
        return JsonUtility.FromJson<TowerSpawnerData>(text);
    }
}
