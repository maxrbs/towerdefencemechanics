using UnityEngine;

public class DirectedShooting : IShootable
{
    private Transform shootingStartPoint;
    private GameObject projectilePrefab;
    private float shootPeriod;
    private float shootingRange;
    private float projectileSpeed;
    private float projectileDamage;
    private float projectileRadius;

    private float nextShotTime = 0f;

    public void Init(TowerData towerData, Transform shootingPoint)
    {
        shootingStartPoint = shootingPoint;

        projectilePrefab = Resources.Load(towerData.ProjectilePath) as GameObject;

        projectileDamage = towerData.ProjectileDamage;
        projectileRadius = towerData.ProjectileRadius;
        projectileSpeed = towerData.ProjectileSpeed;

        shootingRange = towerData.ShootingDistance;
        shootPeriod = towerData.ShootPeriod;
    }


    public void Shoot()
    {
        if (Time.time > nextShotTime)
        {
            nextShotTime = Time.time + shootPeriod;

            GameObjectsPool objectsPool = ServiceLocator.Current.Get<GameObjectsPool>();

            GameObject projectile = objectsPool.Get(projectilePrefab);

            projectile.transform.position = shootingStartPoint.position;
            projectile.transform.rotation = shootingStartPoint.rotation;

            Projectile projectileComponent = projectile.GetComponent<Projectile>();
            projectileComponent.InitComponents();
            projectileComponent.Setup(projectileDamage, projectileRadius, projectileSpeed);

        }
    }

    public float GetShootingRange()
    {
        return shootingRange;
    }

    public Transform GetShootingStartPoint()
    {
        return shootingStartPoint;
    }

    public float GetProjectileSpeed()
    {
        return projectileSpeed;
    }
}
