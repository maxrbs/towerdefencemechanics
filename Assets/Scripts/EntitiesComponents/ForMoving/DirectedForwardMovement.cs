using UnityEngine;

public class DirectedForwardMovement : IMovable
{
    private Transform ownerTransform;

    private float speed;

    public DirectedForwardMovement(Transform ownerTransform)
    {
        this.ownerTransform = ownerTransform;
    }

    public void Init(float speed)
    { 
        this.speed = speed;
    }

    public Vector3 GetMovingDirection()
    {
        return Vector3.forward;
    }

    public float GetSpeed()
    {
        return speed;
    }

    public void Move()
    {
        ownerTransform.Translate(speed * Time.deltaTime * Vector3.forward);
    }

    public void SetSpeed(float newSpeed)
    {
        speed = newSpeed;
    }
}
