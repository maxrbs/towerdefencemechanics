using UnityEngine;

public class SimpleHealthComponent : IDamageable
{
    private Transform ownerTransform;

    private float startHealth;
    private float health;

    public SimpleHealthComponent(Transform ownerTransform)
    {
        this.ownerTransform = ownerTransform;
    }

    public void Init(float startHP)
    {
        startHealth = startHP;
        ResetHP();
    }


    public void ApplyDamage(float damageValue)
    {
        health -= damageValue;

        EffectsPlayer effectsPlayer = ServiceLocator.Current.Get<EffectsPlayer>();

        if (health <= 0f)
        {
            effectsPlayer?.PlayEffect(EffectsPlayer.Effects.Death, ownerTransform.position);

            DestroySelf();
        }
        else
        {
            effectsPlayer?.PlayEffect(EffectsPlayer.Effects.Blood, ownerTransform.position);
        }

    }

    public float GetHP()
    {
        return health;
    }

    public void ResetHP()
    {
        health = startHealth;
    }

    public void DestroySelf()
    {
        GameObjectsPool objectPool = ServiceLocator.Current.Get<GameObjectsPool>();
        objectPool.Release(ownerTransform.gameObject);
        
        //Object.Destroy(ownerTransform.gameObject);
    }
}
