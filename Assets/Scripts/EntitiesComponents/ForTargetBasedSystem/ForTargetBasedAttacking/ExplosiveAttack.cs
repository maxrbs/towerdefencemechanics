using UnityEngine;

public class ExplosiveAttack : ITargetBasedAttackable
{
    private Transform ownerTransform;

    private float damage;
    private float explodeRadius;

    public ExplosiveAttack(Transform ownerTransform)
    {
        this.ownerTransform = ownerTransform;
    }

    public void Init(float damage, float explodeRadius)
    {
        this.damage = damage;
        this.explodeRadius = explodeRadius;
    }


    public void Attack(Transform target)
    {
        if (target.TryGetComponent(out Enemy enemyComponent))
            enemyComponent.GetHealthComponent().ApplyDamage(damage);

        if (target.TryGetComponent(out TargetToDefend targetDefendComponent))
            targetDefendComponent.GetHealthComponent().ApplyDamage(damage);

        DestroySelf();
    }

    public float GetAttackMinDistance()
    {
        return explodeRadius;
    }

    public void SetDamage(float damage)
    {
        this.damage = damage;
    }

    private void DestroySelf()
    {
        GameObjectsPool objectPool = ServiceLocator.Current.Get<GameObjectsPool>();
        objectPool.Release(ownerTransform.gameObject);

        //Object.Destroy(ownerTransform.gameObject);
    }
}
