using UnityEngine;

public class DirectToTargetMovement : ITargetBasedMovable
{
    private Transform ownerTransform;

    private Transform target;
    private float speed;

    private Vector3 direction;

    public DirectToTargetMovement(Transform ownerTransform)
    {
        this.ownerTransform = ownerTransform;
    }

    public void Init(float speed)
    {   
        this.speed = speed;
    }

    public void Move()
    {
        direction = target.position - ownerTransform.position;
        direction = direction.normalized;

        ownerTransform.rotation = Quaternion.LookRotation(direction);
        ownerTransform.Translate(speed * Time.deltaTime * Vector3.forward);
    }

    public void SetTarget(Transform target)
    {
        this.target = target;
    }

    public Vector3 GetMovingDirection() 
    {
        return direction;
    }

    public float GetSpeed()
    {
        return speed;
    }

    public void SetSpeed(float speed)
    {
        this.speed = speed;
    }

    public void SetOwner(Transform owner)
    {
        ownerTransform = owner;
    }
}
