using UnityEngine;
using UnityEngine.AI;

public class NavMeshTargetMovement : ITargetBasedMovable
{
    private Transform ownerTransform;
    
    private NavMeshAgent navMeshAgentComponent;

    private Transform target;
    private float speed;

    public NavMeshTargetMovement(Transform ownerTransform)
    {
        this.ownerTransform = ownerTransform;
    }

    public void Init(float speed, NavMeshAgent navMeshAgent)
    {
        SetNavMeshComponent(navMeshAgent);
        SetSpeed(speed);
    }

    public void Move()
    {        
        navMeshAgentComponent.SetDestination(target.position);
    }

    public void SetTarget(Transform target)
    {
        this.target = target;
    }

    public float GetSpeed()
    {
        return speed;
    }

    public void SetSpeed(float speed)
    {
        this.speed = speed;
        navMeshAgentComponent.speed = speed;
    }

    public Vector3 GetMovingDirection()
    {
        return ownerTransform.forward;
    }

    private void SetNavMeshComponent(NavMeshAgent navMeshComp)
    { 
        navMeshAgentComponent = navMeshComp;

        navMeshAgentComponent.Warp(ownerTransform.position);
    }
}
