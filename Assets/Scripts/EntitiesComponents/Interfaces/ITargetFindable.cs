using UnityEngine;

public interface ITargetFindable
{
    public Transform GetTarget(int index);

    public Transform GetClosestTo(Vector3 position);

    public int GetTargetsCount();
}
