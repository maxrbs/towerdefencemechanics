using UnityEngine;

public interface ITargetBasedAttackable
{
    public void Attack(Transform target);

    public float GetAttackMinDistance();

    public void SetDamage(float damage);
}
