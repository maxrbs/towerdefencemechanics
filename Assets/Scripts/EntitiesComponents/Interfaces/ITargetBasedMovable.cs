using UnityEngine;

public interface ITargetBasedMovable : IMovable
{
    public void SetTarget(Transform target);
}
