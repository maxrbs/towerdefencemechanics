public interface IDamageable
{
    public void ApplyDamage(float damageValue);

    public float GetHP();

    public void ResetHP();
}
