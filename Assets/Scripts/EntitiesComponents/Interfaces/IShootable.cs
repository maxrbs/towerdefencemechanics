using UnityEngine;

public interface IShootable
{
    public void Shoot();

    public float GetShootingRange();
    
    public float GetProjectileSpeed();

    public Transform GetShootingStartPoint();
}
