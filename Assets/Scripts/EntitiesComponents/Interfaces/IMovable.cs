using UnityEngine;

public interface IMovable
{
    public void Move();

    public void SetSpeed(float newSpeed);

    public float GetSpeed();

    public Vector3 GetMovingDirection();
}
