using UnityEngine;

public interface IAimCalculatable
{
    public Quaternion CalculateAimDirection(Transform target);

    public Vector3 CalculateTouchPoint(Transform target);
}
