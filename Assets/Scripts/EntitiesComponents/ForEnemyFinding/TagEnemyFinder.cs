using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TagEnemyFinder : ITargetFindable
{
    private Transform ownerTransform;
    private float searchDistance;
    private string tag;
    public void Init(float searchDistance, string tag)
    {
        this.searchDistance = searchDistance; 
        this.tag = tag;
    }


    public TagEnemyFinder(Transform ownerTransform)
    {
        this.ownerTransform = ownerTransform;
    }

    public Transform GetClosestTo(Vector3 position)
    {
        List<GameObject> enemies = GetEnemiesList();

        Transform closestTransform = enemies[0].transform;
        float minDist = Vector3.Distance(enemies[0].transform.position, ownerTransform.position);

        foreach (GameObject enemy in enemies) 
        {
            float distance = Vector3.Distance(enemy.transform.position, ownerTransform.position);
            
            if (distance < minDist)
            {

                closestTransform = enemy.transform;
                minDist = distance;
            }
        }

        return closestTransform;
    }

    public Transform GetTarget(int index)
    {
        List<GameObject> enemies = GetEnemiesList();

        return enemies[index].transform;
    }

    public int GetTargetsCount()
    {
        List<GameObject> enemies = GetEnemiesList();
        return enemies.Count;
    }

    private List<GameObject> GetEnemiesList()
    {
        List<GameObject> enemies = GameObject.FindGameObjectsWithTag(tag).ToList();
        enemies = enemies.Where(enemy => enemy.gameObject.activeSelf == true).ToList();
        enemies = enemies.Where(enemy => Vector3.Distance(enemy.transform.position, ownerTransform.position) < searchDistance).ToList();
        return enemies;
    }
}
