using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PoolEnemiesFinder : ITargetFindable
{
    private Transform ownerTransform;
    private GameObjectsPool objectsPool;
    private float range;

    public PoolEnemiesFinder(Transform ownerTransform)
    {
        this.ownerTransform = ownerTransform;
    }

    public void Init(float range)
    {
        this.range = range;
        objectsPool = ServiceLocator.Current.Get<GameObjectsPool>();
    }


    public Transform GetClosestTo(Vector3 position)
    {
        Transform closest = null;

        float minDistance = range;

        List<Enemy> enemies = objectsPool.GetContainer().GetComponentsInChildren<Enemy>().ToList();
        enemies = enemies.Where(enemy => enemy.gameObject.activeSelf == true).ToList();
        
        foreach (Enemy enemy in enemies) 
        { 
            float distance = Vector3.Distance(enemy.transform.position, position);
            
            if (distance < minDistance)
            {
                closest = enemy.transform;

                minDistance = distance;
            }
        }

        return closest;
    }

    public Transform GetTarget(int index)
    {
        int currentIndex = 0;

        List<Enemy> enemies = objectsPool.GetContainer().GetComponentsInChildren<Enemy>().ToList();
        enemies = enemies.Where(enemy => enemy.gameObject.activeSelf == true).ToList();
        return enemies[index].transform;



        foreach (Transform child in objectsPool.GetContainer())
        {
            if (child == ownerTransform)
                continue;

            if (child.gameObject.activeSelf == false)
                continue;

            float distance = Vector3.Distance(child.position, ownerTransform.position);

            if (distance > range)
                continue;

            if (child.gameObject.TryGetComponent(out Enemy en))
                if (currentIndex == index)
                    return child;
                else
                    currentIndex++;
        }

        return null;
    }

    public int GetTargetsCount()
    {
        int count = 0;
        foreach (Transform child in objectsPool.GetContainer())
        {
            if (child.gameObject.activeSelf && child.GetComponent<Enemy>() == true)
                count++;
        }

        return count;
    }
}
