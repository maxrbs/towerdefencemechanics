using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class ByCollisionEntryTargetFinder : MonoBehaviour, ITargetFindable
{
    private List<Transform> collidedEnemies;

    private void Start()
    {
        collidedEnemies = new List<Transform>();
    }

    private void OnTriggerEnter(Collider other)
    {
        Transform otherObject = other.transform;

        if (otherObject.GetComponent<Enemy>() != null)
            if (!collidedEnemies.Contains(otherObject))
                collidedEnemies.Add(otherObject);
    }

    private void OnTriggerExit(Collider other)
    {
        Transform otherObject = other.transform;

        if (otherObject.GetComponent<Enemy>() != null)
            collidedEnemies.Remove(otherObject);
    }

    public Transform GetTarget(int index)
    {
        if (GetTargetsCount() > index)
            return collidedEnemies[index];
        else
            return null;
    }

    public int GetTargetsCount()
    {
        collidedEnemies = collidedEnemies.Where(item => item != null).ToList();
        return collidedEnemies.Count;
    }

    public Transform GetClosestTo(Vector3 position)
    {
        if (GetTargetsCount() < 1)
            return null;

        Transform closest = collidedEnemies[0];
        float minDistance = Vector3.Distance(closest.position, position);

        foreach (var item in collidedEnemies)
        {
            float itemDistance = Vector3.Distance(item.position, position);

            if (itemDistance < minDistance)
            {
                closest = item;
                minDistance = itemDistance;
            }
        }

        return closest;
    }
}
