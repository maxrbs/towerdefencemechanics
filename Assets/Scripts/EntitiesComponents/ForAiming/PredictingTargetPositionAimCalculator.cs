using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PredictingTargetPositionAimCalculator : IAimCalculatable
{
    private const float ACCURACY = 0.5f;

    private IShootable shootable;

    public PredictingTargetPositionAimCalculator()
    {    }

    public void Init(IShootable shootable)
    {
        this.shootable = shootable;
    }

    public Quaternion CalculateAimDirection(Transform target)
    {
        Vector3 startPoint = shootable.GetShootingStartPoint().position;
        Vector3 endPoint = CalculateTouchPoint(target);

        Vector3 direction = endPoint - startPoint;

        Quaternion directionQuaternion = Quaternion.LookRotation(direction);

        return directionQuaternion;
    }

    public Vector3 CalculateTouchPoint(Transform target)
    {
        Vector3 startPoint = shootable.GetShootingStartPoint().position;

        Vector3 targetOffset = new Vector3(0f, 0.7f, 0f);

        Vector3 cannonBallPos = startPoint;
        Vector3 targetPos = target.position + targetOffset;

        var targetMoveComp = target.GetComponent<Enemy>().GetMoveComponent();

        float projectileSpeed = shootable.GetProjectileSpeed();

        float multiplier = 0.5f;
        float passedTime = 0f;

        for (int i = 0; i < 10; i++)
        {
            float timeToReach = Vector3.Distance(cannonBallPos, targetPos) / projectileSpeed; 

            float partedTime = timeToReach * multiplier;
            passedTime += partedTime;

            targetPos = targetPos + targetMoveComp.GetMovingDirection() * targetMoveComp.GetSpeed() * partedTime;

            Vector3 direction = targetPos - startPoint;

            cannonBallPos = startPoint + direction.normalized * projectileSpeed * passedTime;

            float distance = Vector3.Distance(cannonBallPos, targetPos);
            
            if (distance < ACCURACY)
                break;            
        }

        return targetPos;

    }

    public void InitShootingComponent(IShootable shootable)
    {
        this.shootable = shootable;
    }
}
