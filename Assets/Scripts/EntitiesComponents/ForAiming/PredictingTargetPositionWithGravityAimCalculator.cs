﻿using UnityEngine;

public class PredictingTargetPositionWithGravityAimCalculator : IAimCalculatable
{
    private IShootable shootable;
    
    private bool useLobAngle;

    public PredictingTargetPositionWithGravityAimCalculator()
    {    }

    public void Init(IShootable shootable, bool useLobAngle)
    {
        this.shootable = shootable;
        useLobAngle = false;
    }


    public Quaternion CalculateAimDirection(Transform target)
    {
        Vector3 startPoint = shootable.GetShootingStartPoint().position;
        Vector3 endPoint = CalculateTouchPoint(target);
        float projectileSpeed = shootable.GetProjectileSpeed();
        
        float flightTime = GetTravelTime(startPoint, endPoint, projectileSpeed);

        Vector3 direction = (endPoint - startPoint)/ flightTime - Physics.gravity * flightTime / 2f;

        Quaternion directionQuaternion = Quaternion.LookRotation(direction);

        return directionQuaternion;
    }

    public Vector3 CalculateTouchPoint(Transform target)
    {
        Vector3 startPos = shootable.GetShootingStartPoint().position;

        Vector3 targetOffset = new Vector3(0f, 0f, 0f);
        Vector3 targetPos = target.position + targetOffset;

        var targetMoveComp = target.GetComponent<Enemy>().GetMoveComponent();
                
        float projectileSpeed = shootable.GetProjectileSpeed();

        float flightTime = GetTravelTime(startPos, targetPos, projectileSpeed);

        targetPos = targetPos + targetMoveComp.GetMovingDirection() * targetMoveComp.GetSpeed() * flightTime;

        return targetPos;
    }

    private float GetTravelTime(Vector3 startPoint, Vector3 endPoint, float projectileSpeed)
    {
        Vector3 direction = endPoint - startPoint;

        float flightTime;

        float gravitySquared = Physics.gravity.sqrMagnitude;

        float b = projectileSpeed * projectileSpeed + Vector3.Dot(direction, Physics.gravity);
        float discriminant = b * b - gravitySquared * direction.sqrMagnitude;

        if (discriminant < 0)
        {
            flightTime = Mathf.Sqrt((b) * 2f / gravitySquared);

            return flightTime;
        }
       
        if (useLobAngle)
            flightTime = Mathf.Sqrt((b + Mathf.Sqrt(discriminant)) * 2f / gravitySquared);
        else
            flightTime = Mathf.Sqrt((b - Mathf.Sqrt(discriminant)) * 2f / gravitySquared);


        return flightTime;
    }
}