using UnityEngine;

public class CurrentTargetPositionAimCalculator : IAimCalculatable
{
    private IShootable shootable;

    public CurrentTargetPositionAimCalculator()
    {    }

    public void Init(IShootable shootable)
    { 
        this.shootable = shootable;
    }

    public Quaternion CalculateAimDirection(Transform target)
    {
        Vector3 direction = target.position - shootable.GetShootingStartPoint().position;

        Quaternion directionQuaternion = Quaternion.LookRotation(direction);

        return directionQuaternion;
    }

    public Vector3 CalculateTouchPoint(Transform target)
    {
        return target.position;
    }

    public void InitShootingComponent(IShootable shootable)
    {
        this.shootable = shootable;
    }
}
