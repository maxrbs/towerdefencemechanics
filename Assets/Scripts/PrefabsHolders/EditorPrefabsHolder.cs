using System.Collections.Generic;
using UnityEngine;

public class EditorPrefabsHolder : MonoBehaviour, IPrefabsHolder
{
    [SerializeField] private List<GameObject> prefabsList;

    public GameObject GetByIndex(int index)
    {
        return prefabsList[index];
    }

    public GameObject GetRandom()
    {
        int randomValue = Random.Range(0, GetPrefabsCount());

        return prefabsList[randomValue];
    }

    public int GetPrefabsCount()
    {
        return prefabsList.Count;
    }

}
