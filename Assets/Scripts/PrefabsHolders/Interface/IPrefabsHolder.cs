using UnityEngine;

public interface IPrefabsHolder
{
    public GameObject GetByIndex(int index);

    public GameObject GetRandom();

    public int GetPrefabsCount();
}
