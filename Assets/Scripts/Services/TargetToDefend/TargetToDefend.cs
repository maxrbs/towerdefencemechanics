using UnityEngine;

public class TargetToDefend : MonoBehaviour, IService
{
    private IDamageable healthComponent;

    public TargetToDefend()
    {    }

    public void Init(DefencingTargetData data)
    {
        healthComponent = new SimpleHealthComponent(transform);
        (healthComponent as SimpleHealthComponent).Init(data.startHP);

        transform.position = new Vector3(data.x, data.y, data.z);
    }

    public IDamageable GetHealthComponent()
    {
        return healthComponent;
    }

}
