using UnityEngine;

public class PoolObjectSimpleReleaser : MonoBehaviour
{
    private float nextTriggerTime = 0f;
    private bool isNeedToTrigger = false;

    public void ReleaseAfter(float seconds)
    {
        nextTriggerTime = Time.time + seconds;
        isNeedToTrigger = true;
    }

    private void Update()
    {
        if (isNeedToTrigger && Time.time > nextTriggerTime)
            ReleaseObject();
    }

    private void ReleaseObject()
    {
        ServiceLocator.Current.Get<GameObjectsPool>().Release(gameObject);
        Destroy(this);
    }
}
