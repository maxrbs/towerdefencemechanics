using UnityEngine;

public class GameObjectsPool : IService
{
    private Transform poolContainer;

    public GameObjectsPool(Transform poolContainer)
    {
        this.poolContainer = poolContainer;
    }

    public Transform GetContainer()
    { 
        return poolContainer;
    }

    public GameObject Get(GameObject prefab)
    {
        foreach (Transform child in poolContainer)
        {
            if (child.name == prefab.name && child.gameObject.activeSelf == false)
            {
                child.gameObject.SetActive(true);

                return child.gameObject;
            }
        }

        return CreateObject(prefab);
    }

    public GameObject CreateObject(GameObject prefab)
    {
        GameObject obj = GameObject.Instantiate(prefab, poolContainer).gameObject;
        obj.name = prefab.name;
        return obj;
    }

    public void Release(GameObject obj)
    {
        obj.gameObject.SetActive(false);
    }    
}
