using System.Collections.Generic;

public class ServiceLocator
{
    private ServiceLocator() { }

    public static ServiceLocator Current { get; set; }

    public static void Init()
    { 
        Current = new ServiceLocator();
    }

    private readonly Dictionary<string, IService> services = new Dictionary<string, IService>();

    public T Get<T>() where T : IService
    { 
        string key = typeof(T).Name;

        if (!services.ContainsKey(key))        
            throw new System.Exception($"Cant find {key} in Services!");
                    
        return (T)services[key];
    }

    public void Register<T>(T service) where T : IService 
    {
        string key = typeof(T).Name;

        if (services.ContainsKey(key))
            return;

        services.Add(key, service);
    }

    public void Unregister<T>() where T : IService 
    {
        string key = typeof(T).Name;

        if (!services.ContainsKey(key))
            return;

        services.Remove(key);
    }

}
