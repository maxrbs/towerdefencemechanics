using System.Collections.Generic;
using UnityEngine;

public class EffectsPlayer : IService
{
    public enum Effects
    {
        Blood,
        Death,
        Lightning
    }

    private string effectsPath = "Effects/";

    private Dictionary<Effects, string> effectsMap  = new Dictionary<Effects, string>()
    {
        {Effects.Blood, "Blood" },
        {Effects.Death, "Death" },
        {Effects.Lightning, "Lightning" },
    };

    public void PlayEffect(Effects effect, Vector3 position, float time = 0.3f)
    {
        string effectPath = effectsPath + effectsMap[effect];
        GameObject effectToSpawn = Resources.Load(effectPath) as GameObject;

        GameObject spawnedEffect = ServiceLocator.Current.Get<GameObjectsPool>().Get(effectToSpawn);

        spawnedEffect.transform.position = position;
        spawnedEffect.GetComponent<ParticleSystem>().Play(true);

        spawnedEffect.AddComponent<PoolObjectSimpleReleaser>().ReleaseAfter(time);
    }
}
