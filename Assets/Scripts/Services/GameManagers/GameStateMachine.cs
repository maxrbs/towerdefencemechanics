using UnityEngine;

public enum State
{ 
    Playing,
    Paused,
}

public class GameStateMachine : IService
{
    [SerializeField] private State state;

    public GameStateMachine()
    {    }

    public void Init(State state)
    {
        this.state = state;
    }

    public State GetState()
    {
        return state;
    }

    public void SetState(State state) 
    {
        this.state = state;
    }
}
