using UnityEngine;

public class ServiceLoader_testScene : MonoBehaviour
{
    [SerializeField] private TargetToDefend targetToDefend;
    [SerializeField] private EnemySpawner enemySpawner;
    [SerializeField] private JsonFromResourcesLoader configLoader;

    private TowerSpawner towerInstantSpawner;
    private GameObjectsPool gameObjectsPool;
    private GameStateMachine gameStateMachine;
    private EffectsPlayer effectPlayer;

    private EnemySpawnerData enemySpawnerData;
    private TowerSpawnerData towerSpawnerData;
    private DefencingTargetData defencingTargetData;


    private void Awake()
    {
        //����� ������ �������
        GetConfigs();

        CreateServices();
        RegisterServices();
        InitServices();

        gameStateMachine.SetState(State.Playing);
    }

    private void GetConfigs()
    {
        enemySpawnerData = configLoader.GetEnemySpawnerData();
        towerSpawnerData = configLoader.GetTowerSpawnerData();
        defencingTargetData = configLoader.GetDefencingTargetData();
    }

    private void RegisterServices()
    {
        ServiceLocator.Init();

        ServiceLocator.Current.Register<GameObjectsPool>(gameObjectsPool);
        ServiceLocator.Current.Register<EnemySpawner>(enemySpawner);
        ServiceLocator.Current.Register<TowerSpawner>(towerInstantSpawner);
        ServiceLocator.Current.Register<TargetToDefend>(targetToDefend);
        ServiceLocator.Current.Register<GameStateMachine>(gameStateMachine);
        ServiceLocator.Current.Register<EffectsPlayer>(effectPlayer);

    }

    private void CreateServices()
    {
        effectPlayer = new EffectsPlayer();
        gameObjectsPool = new GameObjectsPool(transform);
        gameStateMachine = new GameStateMachine();
        towerInstantSpawner = new TowerSpawner();
    }

    private void InitServices()
    {
        gameStateMachine.Init(State.Paused);

        enemySpawner.Init(enemySpawnerData);
        targetToDefend.Init(defencingTargetData);
        towerInstantSpawner.Init(towerSpawnerData);


        towerInstantSpawner.SpawnTowers(); // ���������
    }
}