﻿using UnityEngine;

public abstract class Enemy : MonoBehaviour
{
    protected Transform targetToAttack;

    protected IDamageable healthComponent;
    protected ITargetBasedMovable moveComponent;
    protected ITargetBasedAttackable attackComponent;

    public abstract void InitComponents();
    public abstract void Setup(EnemyData enemyData);


    private void Update()
    {
        if (targetToAttack == null)
            return;

        if (Vector3.Distance(transform.position, targetToAttack.transform.position) <= attackComponent.GetAttackMinDistance())
            attackComponent.Attack(targetToAttack);
        else
            moveComponent.Move();
    }

    public void SetAttackTarget(Transform targetToAttack)
    {
        moveComponent.SetTarget(targetToAttack);
        this.targetToAttack = targetToAttack;
    }


    public IDamageable GetHealthComponent()
    {
        return healthComponent;
    }

    public ITargetBasedMovable GetMoveComponent() 
    {
        return moveComponent;
    }

    public ITargetBasedAttackable GetAttackComponent() 
    {
        return attackComponent;
    }
}
