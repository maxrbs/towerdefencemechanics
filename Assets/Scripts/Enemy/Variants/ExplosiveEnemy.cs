using UnityEngine.AI;

public class ExplosiveEnemy : Enemy
{
    public override void InitComponents()
    {
        moveComponent = new NavMeshTargetMovement(transform);
        healthComponent = new SimpleHealthComponent(transform);
        attackComponent = new ExplosiveAttack(transform);
    }

    public override void Setup(EnemyData enemyData)
    {
        SetAttackTarget(ServiceLocator.Current.Get<TargetToDefend>().gameObject.transform);

        NavMeshAgent navMeshAgent = gameObject.GetComponent<NavMeshAgent>();
        if (navMeshAgent == null)
            navMeshAgent = gameObject.AddComponent<NavMeshAgent>();

        (moveComponent as NavMeshTargetMovement).Init(enemyData.Speed, navMeshAgent);
        (healthComponent as SimpleHealthComponent).Init(enemyData.StartHealth);
        (attackComponent as ExplosiveAttack).Init(enemyData.Damage, enemyData.AttackDistance);
    }
}
