Переделал архитектуру под работу сервисов. 
На каждую сцену пишется свой serviceLoader, в который помещаются сервисы для работы.
Он после инициализации и регистрации сервисов запускает работу механик.
Также он читает конфиги с соответвующей папки в Resources, передавая оттуда в объекты данные.
При спавне объектов в них создаются NonMonobehaviour объекты-компоненты для работы. 
Monobehaviour объектами остались главенствующие классы (Enemy, CannonTower и т.д.) ввиду использования Update метода.
Для вызова эффектов классы обращаются к ServiceLocator и берут у него нужный сервис, после чего обращаются к нему.
Вместо Instantiate использован ObjectPool как отдельный сервис. Обращение к нему происходит через ServiceLocator.

Что можно исправить:
Добавить EventBus для вызова событий, для большей чистоты кода и отсутсвия надобности в обращении к отдельным сервисам напрямую.
Расширить конфиги и добавить туда значения об эффектах, звуках и т.д.



